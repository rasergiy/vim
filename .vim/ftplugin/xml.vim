set ts=2  " Tabs are 2 spaces
set expandtab
set smarttab
set shiftwidth=2  " Tabs under smart indent

