
set nocompatible
set term=screen-256color 


au BufReadPost *.doc silent %!antiword "%"	" Non text files filter
au BufReadPost *.odt silent %!unzip -p "%" content.xml | o3totxt
au BufReadPost *.pdf silent %!pdftotext -nopgbrk "%" - |fmt -csw78


set showmode				"        +-------------------------------------------------+
set showcmd                             "   num >|1 ### # ###### Vim                               |
set number                              "   ber >|2 ## #### #                                      |
set ruler                               "        |~                                                |
set showtabline=2                       "        |-- ########## ##### --         2f     43,8   17% |
                                        "        +-------------------------------------------------+
                                        "         ^^^^^^^^^^^                  ^^^^^^^^ ^^^^^^^^^^
                                        "           'showmode'                 'showcmd'  'ruler'   
set wildmode=list:longest,full

set backup
set backupdir=~/.vim/backup
set directory=~/.vim/tmp
set wrap linebreak textwidth=0
set timeoutlen=0  			" Time to wait after ESC (default causes an annoying delay)

set t_Co=256
set background=dark
colorscheme leo
set mouse=
syntax enable

set history=500

set ignorecase				" ignore case in search
set hlsearch 				" Mark while search 
set incsearch				" Search while typing

set noautoindent
set nocindent
set smartindent
filetype indent on
filetype plugin on

set showmatch				" matching bracket
set mat=5				" bracket blinking

set wildmenu
set wcm=<Tab>

set fileencodings=utf-8,cp1251,cp866,latin1

set autowrite  				" Writes on make/shell command
set bs=2  				" Backspace over everything in insert mode
set formatoptions=tcq

let g:use_python_after_ftplugin = 1

let g:use_vtreeexplorer="YES"
let g:miniBufExplMapWindowNavVim = 1
let g:miniBufExplMapWindowNavArrows = 1
let g:miniBufExplMapCTabSwitchBufs = 1
let g:miniBufExplModSelTarget = 1
let g:rails_default_file='config/database.yml'


" F8: encoding menu
menu Encoding.windows-1251 :e ++enc=cp1251<CR>
menu Encoding.koi8-r       :e ++enc=koi8-r<CR>
menu Encoding.ibm-866      :e ++enc=ibm866<CR>
menu Encoding.utf-8        :e ++enc=utf-8 <CR>
nmap <F8> :emenu Encoding.<TAB>
imap <F8> <C-O>:emenu Encoding.<TAB>

" F9: Saveto encoding menu
menu SaveToEncoding.windows-1251 :w ++enc=cp1251<CR>
menu SaveToEncoding.koi8-r       :w ++enc=koi8-r<CR>
menu SaveToEncoding.utf-8        :w ++enc=utf-8 <CR>
menu SaveToEncoding.ibm-866      :w ++enc=ibm866<CR>
nmap  <F9> :emenu SaveToEncoding.<TAB>
imap <F8> <C-O>:emenu Encoding.<TAB>


" Turn off  REPLACE mode
imap <Ins> <Nop>
" Do not move cursor back when leave insert mode
imap <Esc> <Esc>l

" Ctrl-Left
nmap Od  B
imap Od <C-O>B
" Alt-Left
imap <M-Left> <C-O>B
nmap <M-Left> B
" Ctrl-Right
imap Oc <C-O>W
nmap Oc W
" Alt-Right
imap <M-Right> <C-O>W
nmap <M-Right> W

" Tab Movement
imap <M-z> <C-O>gT
nmap <M-z> gT
imap <M-x> <C-O>gt
nmap <M-x> gt

" Wrapped strings movement
imap <Up> <C-O>g<Up>
nmap <Up> gk
imap <Down> <C-O>gj
nmap <Down> gj
imap <M-Up> <C-O>k
nmap <M-Up> k
imap <M-Down> <C-O>j
nmap <M-Down> j


" Turn off search highlightning
nmap <F1> :nohlsearch<CR>
imap <F1> <Esc>:nohlsearch<CR>
vmap <F1> <Esc>:nohlsearch<CR>gv

" F2: Save
imap <F2> <C-O>:w<CR>
nmap <F2> :w<CR>

" F3: Tabnew
imap <F3> <C-O>:tabnew<CR>
nmap <F3> :tabnew<CR>

" F4: Split
imap <F4> <C-O>:split<CR>
nmap <F4> :split<CR>

" F5: Select buffer
nmap <F5> :buffers<CR>:buffer<Space>
imap <F5> <Esc>:buffers<CR>:buffer<Space>

" F6: Switch window:
nmap <F6> <C-W>k<C-W>_
imap <F6> <Esc><C-W>k<C-W>_a

" F7: Split window
nmap <F7> <C-W>j<C-W>_
imap <F7> <Esc><C-W>j<C-W>_a

" F10: close
map <F10> :close<CR>
imap <F10> <C-O>:close<CR>

" F11: Syntax off
nmap <F11> :syntax off<CR>
imap <F11> <Esc>:syntax off<CR>a

" F12: ColorScheme off
nmap <F12> :syntax on<CR>:colorscheme leo<CR>
imap <F12> <ESC>:syntax on<CR>:colorscheme leo<CR>a

