" Vim syntax file
" Language:	RSLib HTML template
" Maintainer:	raasergiy <rasergiy@gmail.com>
" Last Change:	2011.12.08

if exists("b:current_syntax")
  finish
endif

if !exists("main_syntax")
  let main_syntax = 'html'
endif

if version < 600
  so <sfile>:p:h/html.vim
else
  runtime! syntax/html.vim
  unlet b:current_syntax
endif

syn keyword htmlSpecialTagName contained ref text fn

let b:current_syntax = "rslibhtml"
