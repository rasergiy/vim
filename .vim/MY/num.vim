:%s/\n/ /
:%s/\s*<p>/\r/g
:function! RscNum()
:    let l:count = line ("$")
:    for n in range (1, l:count)
:        let l:nn = l:n - 1
:        let l:currline = getline (n)
:        let l:currline = printf ( "<p><a name=\"32-15-%d\">%d</a>. %s", l:nn, l:nn, l:currline  )
:        call setline ( l:n, l:currline )
:    endfor
:    unlet l:count
:endfunction
:call RscNum()
