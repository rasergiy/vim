:%s/<a href=".\{-}">\(.\{-}\)<\/a>/\1/gc
:%s/<p><\/p>//g
:%s/<h\(\d\)><a class="an" name="part_\(.\{-}\)">\s*\(.\{-}\)\s*<\/a><\/h\1>/<h\1 id="h\2">\3<\/h\1>/gc 
:%s/<span style="font-style: italic;">\(.\{-}\)<\/span>/<i>\1<\/i>/g
:%s/<div class="section_\d">//gc
:%s/<span style=""> <\/span>/ /g
:%s/<span style="">\(.\{-}\)<\/span>/\1/g
:%s/<\/div>//g
:%s/<\(\/*\)h3/<\1h1/g
:%s/<\(\/*\)h4/<\1h2/g

