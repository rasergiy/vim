:so ~/.vim/MY/mso.vim

:%s/<p>\(\d\+\)\.\s*/<p id='p:\1'>/
:%s/<b>\(\s*\)<\/b>/\1/g
:%s/&quot;\s*<b>/<q>/gc
:%s/<b>\s*&quot;/<q>/gc
:%s/<\/b>[\s.]*&quot;/<\/q>/gc 
:%s/&quot;[\s.]*<\/b>/<\/q>/gc

